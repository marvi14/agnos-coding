import { Item, ITEM_ID } from "../../shared/interfaces/item.interface";
import { cloneDeep, uniqBy, orderBy } from 'lodash';
import { ADD_ITEM_TO_CART, ADD_ONE_ITEM_TO_CART, CHECKOUT, REMOVE_ONE_ITEM_TO_CART, SET_ITEMS, ShopActions } from "./shop.actions";

export interface ShopState {
    items: Item[];
    cart: Item[];
    potentialCombos: Combo[];
    combos: Combo[];
    sales: Sale[];
}

export const initialState: ShopState = {
    items: [],
    cart: [],
    potentialCombos: [],
    combos: [],
    sales: []
};

export interface Combo {
    id: number;
    composition: ITEM_ID[];
    itemToBeDiscounted: ITEM_ID;
    percentageDiscount: number;
}

export interface Sale {
    items: Item[];
    combos: Combo[];
    total: number;
}

const combos: Combo[] = [{
    id: 0,
    composition: [ITEM_ID.BREAD, ITEM_ID.COFEE],
    itemToBeDiscounted: ITEM_ID.COFEE,
    percentageDiscount: 5
}, {
    id: 1,
    composition: [ITEM_ID.BREAD, ITEM_ID.MILK],
    itemToBeDiscounted: ITEM_ID.BREAD,
    percentageDiscount: 10
}, {
    id: 2,
    composition: [ITEM_ID.JUICE, ITEM_ID.MILK],
    itemToBeDiscounted: ITEM_ID.MILK,
    percentageDiscount: 100
}];

function getClone(state: ShopState): ShopState {
    //BIG NGRX GOTCHA - spread operator it only does a shallow copy so if the object has nested objects you get references not an actual clone
    return cloneDeep(state);
}

export function ShopReducer(state: ShopState = initialState, action: ShopActions) {
    let clone = getClone(state);
    const cart = clone.cart;
    const items = clone.items;

    switch (action.type) {
        case SET_ITEMS:
            clone = { ...clone, items: action.payload };
            break;
        case ADD_ITEM_TO_CART:
            const updatedItem = items.find(item => item.id === action.payload.id);
            const alreadyAdded = cart.find(item => item.id === action.payload.id);

            if (alreadyAdded)
                alreadyAdded.selectedAmount += action.payload.selectedAmount;
            else
                cart.push(action.payload);

            updatedItem.amount -= action.payload.selectedAmount;
            updatedItem.amount > 0 ? updatedItem.selectedAmount = 1 : updatedItem.selectedAmount = 0;
            clone = { ...clone, cart, items };
            break;
        case ADD_ONE_ITEM_TO_CART:
            let cartItem = cart.find(item => item.id === action.payload);
            let itemToUpdate = items.find(item => item.id === action.payload);
            if (itemToUpdate.amount > 0) {
                cartItem.selectedAmount++;
                itemToUpdate.amount--;
            }
            clone = { ...clone, cart, items };
            break;
        case REMOVE_ONE_ITEM_TO_CART:
            cartItem = cart.find(item => item.id === action.payload);
            itemToUpdate = items.find(item => item.id === action.payload);
            if (cartItem.selectedAmount > 0) {
                cartItem.selectedAmount--;
                itemToUpdate.amount++;
                itemToUpdate.selectedAmount = 1;
                if (cartItem.selectedAmount === 0)
                    cart.splice(cart.findIndex(item => item.id === cartItem.id), 1);
            }
            clone = { ...clone, cart, items };
            break;
        case CHECKOUT:
            clone.sales.push({
                combos: clone.combos,
                items: cart,
                total: action.payload
            });
            clone.combos = [];
            clone.cart = [];
            clone.potentialCombos = [];
    }
    checkCombos(clone);
    return clone;
}

function checkCombos(clone: ShopState) {
    if (clone && clone.cart) {
        clone.potentialCombos = [];
        clone.combos = [];
        if (clone.cart.length > 0) {
            clone.cart.forEach(item => {
                const availableCombos = combos.filter(combo => combo.composition.some(comboItem => comboItem === item.id));
                clone.potentialCombos = clone.potentialCombos.concat(availableCombos);
            });
            clone.potentialCombos = orderBy(uniqBy(clone.potentialCombos, (combo) => combo.id), 'percentageDiscount', 'desc');

            if (clone.cart.length >= 2) {
                //Assuming combos are only made by 2 items
                combos.forEach(combo => {
                    const comboCompleted = clone.cart.some(item => item.id === combo.composition[0]) && clone.cart.some(item => item.id === combo.composition[1]);
                    if (comboCompleted) {
                        //For each item of the new combo, look for already added combos to compare the discount
                        combo.composition.forEach(item => {
                            const comboWithItem = clone.combos.findIndex(addedCombo => addedCombo.composition.some(addedItem => addedItem === item));
                            if (comboWithItem === -1)
                                clone.combos.push(combo);
                            else {
                                //Discounts/Free offers cannot overlap/be combined - that's why we always select the best one
                                if (clone.combos[comboWithItem].percentageDiscount < combo.percentageDiscount)
                                    clone.combos.push(combo);
                            }
                        });
                    }
                });
                if (clone.combos.length > 0) {
                    clone.potentialCombos = clone.potentialCombos.filter(potentialCombo => !clone.combos.some(combo => combo.id === potentialCombo.id)).filter(potentialCombo => {
                        for (let i = 0; i < potentialCombo.composition.length; i++) {
                            if (clone.combos.some(combo => combo.composition.some(id => id === potentialCombo.composition[i]) && potentialCombo.percentageDiscount < combo.percentageDiscount))
                                return false;
                        }
                        return true;
                    });
                    const filteredCombos: Combo[] = [];
                    clone.combos = orderBy(uniqBy(clone.combos, (combo) => combo.id), 'percentageDiscount', 'desc');
                    clone.combos.forEach(combo => {
                        for (let i = 0; i < combo.composition.length; i++) {
                            if (!filteredCombos.some(filteredCombo => filteredCombo.id === combo.id)) {
                                const comboWithItem = clone.combos.find(checkedCombo => checkedCombo.id !== combo.id && checkedCombo.composition.some(addedItem => addedItem === combo.composition[i]))
                                if (!comboWithItem) {
                                    if (canAddToCombos(filteredCombos, combo))
                                        filteredCombos.push(combo);
                                }
                                else {
                                    //Discounts/Free offers cannot overlap/be combined - that's why we always select the best one
                                    if (combo.percentageDiscount > comboWithItem.percentageDiscount && canAddToCombos(filteredCombos, combo))
                                        filteredCombos.push(combo);
                                }
                            }
                        }
                    });
                    clone.combos = filteredCombos;
                }
            }
        }
    }
}

function canAddToCombos(potentialCombos, combo): boolean {
    return !potentialCombos.some(filteredCombo => filteredCombo.composition.some(item => item === combo.composition[0])) &&
        !potentialCombos.some(filteredCombo => filteredCombo.composition.some(item => item === combo.composition[1]))
}
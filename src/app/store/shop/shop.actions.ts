import { Action } from '@ngrx/store';
import { Item } from '../../shared/interfaces/item.interface';

export const SET_ITEMS = '[SHOP] SET_ITEMS';
export const ADD_ITEM_TO_CART = '[SHOP] ADD_ITEM_TO_CART';
export const ADD_ONE_ITEM_TO_CART = '[SHOP] ADD_ONE_ITEM_TO_CART';
export const REMOVE_ONE_ITEM_TO_CART = '[SHOP] REMOVE_ONE_ITEM_TO_CART';
export const CHECKOUT = '[SHOP] CHECKOUT';

export class SetItems implements Action {
    readonly type = SET_ITEMS;
    constructor(public payload: Item[]) { }
}

export class AddItemToCart implements Action {
    readonly type = ADD_ITEM_TO_CART;
    constructor(public payload: Item) { }
}

export class AddOneToItemInCart implements Action {
    readonly type = ADD_ONE_ITEM_TO_CART;
    constructor(public payload: number) { }
}

export class RemoveOneToItemInCart implements Action {
    readonly type = REMOVE_ONE_ITEM_TO_CART;
    constructor(public payload: number) { }
}

export class Checkout implements Action {
    readonly type = CHECKOUT;
    constructor(public payload: number) { }
}

export type ShopActions = SetItems | AddItemToCart | AddOneToItemInCart | RemoveOneToItemInCart | Checkout;
import { ActionReducerMap, createFeatureSelector, createSelector } from "@ngrx/store";
import { ShopAnalytics } from "../shared/interfaces/shop.analytics";
import { ShopReducer, ShopState } from "./shop/shop.reducer";

export interface AppState {
    shopState: ShopState;
}

export const reducers: ActionReducerMap<AppState> = {
    shopState: ShopReducer,
};

//NG-RX Selectors that makes querys to the store 
export const getAppState = createFeatureSelector<AppState>('app');
export const getItems = createSelector(getAppState, (state: AppState) => state.shopState.items);
export const getAnalytics = createSelector(getAppState, (state: AppState) => {
    return <ShopAnalytics>{
        stock: state.shopState.items.reduce((total, { amount }) => total + amount, 0),
        revenue: state.shopState.sales.reduce((totalRevenue, { total }) => total + totalRevenue, 0),
        sales: state.shopState.sales.length
    };
});
export const getCart = createSelector(getAppState, (state: AppState) => state.shopState.cart);
export const getPotentialCombos = createSelector(getAppState, (state: AppState) => {
    return { potentialCombos: state.shopState.potentialCombos, combos: state.shopState.combos };
});
export const getSales = createSelector(getAppState, (state: AppState) => state.shopState.sales);
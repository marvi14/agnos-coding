import { Component, OnDestroy, OnInit } from '@angular/core';
import { ItemsService } from '../../shared/services/items.service';
import { Item } from '../../shared/interfaces/item.interface';
import { take } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { Store } from '@ngrx/store';
import * as appState from '../../store/reducers.index';
import { ShopAnalytics } from '../../shared/interfaces/shop.analytics';
import { AddItemToCart, SetItems } from 'app/store/shop/shop.actions';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { cloneDeep } from 'lodash';
import { Combo } from '../../store/shop/shop.reducer';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'dashboard-cmp',
  moduleId: module.id,
  templateUrl: 'dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})

export class DashboardComponent implements OnInit, OnDestroy {

  public items: Item[] = [];
  public cart: Item[] = [];
  public analytics: ShopAnalytics = {
    stock: 0,
    revenue: 0,
    sales: 0
  };
  public potentialCombos: Combo[] = [];
  public combos: Combo[] = [];

  constructor(private itemsService: ItemsService, private spinner: NgxSpinnerService, private _store: Store<appState.AppState>, private translate: TranslateService) {
    this._store.select(appState.getAnalytics).pipe(untilDestroyed(this)).subscribe((analytics: ShopAnalytics) => {
      this.analytics = analytics;
    });

    this._store.select(appState.getItems).pipe(untilDestroyed(this)).subscribe(items => {
      this.items = cloneDeep(items);
    });

    this._store.select(appState.getPotentialCombos).pipe(untilDestroyed(this)).subscribe(combosData => {
      this.potentialCombos = combosData.potentialCombos;
      this.combos = combosData.combos;
    });

    this._store.select(appState.getCart).pipe(untilDestroyed(this)).subscribe(cart => {
      this.cart = cloneDeep(cart);
    });
  }

  ngOnInit() {
    if (this.items.length === 0) {
    this.spinner.show();
    this.itemsService.getItems().pipe(take(1)).subscribe((items: Item[]) => {
      //Simulates the timing for the API Call to show the loading spinner
      setTimeout(() => {
        this.items = items;
        this._store.dispatch(new SetItems(items));
        this.spinner.hide();
      }, 1000);
    });
  }
  }

  public addItemsToCart(item: Item) {
    if (item.amount > 0)
      this._store.dispatch(new AddItemToCart(item));
  }

  public checkPotentialCombos(item) {
    const potentialCombos = this.potentialCombos.filter(combo => combo.composition.some(comboItem => comboItem === item.id));
    if (potentialCombos.length === 0)
      return '';
    else {
      const bestCombo = potentialCombos[0];
      if (bestCombo.itemToBeDiscounted === item.id) {
        const complement = this.items.find(listItem => listItem.id === bestCombo.composition.find(comboItemId => comboItemId !== item.id));
        return bestCombo.percentageDiscount !== 100 ? this.translate.instant('DASHBOARD.DISCOUNT_CARD_TEXT', { discount: bestCombo.percentageDiscount, product: this.translate.instant(complement.name) }) : this.translate.instant('DASHBOARD.FREE_CARD_TEXT', { product: this.translate.instant(complement.name) });
      }
    }
  }

  ngOnDestroy() { }
}

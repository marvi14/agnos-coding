import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalesComponent } from './sales.component';

export const SalesRoutingDefinitions: Routes = [
    { path: '', redirectTo: 'list', pathMatch: 'full' },
    { path: 'list', component: SalesComponent },
];

@NgModule({
    imports: [RouterModule.forChild(SalesRoutingDefinitions)],
    exports: [RouterModule],
})
export class SalesRoutingModule { }
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Sale } from 'app/store/shop/shop.reducer';
import { untilDestroyed } from 'ngx-take-until-destroy';
import * as appState from '../../store/reducers.index';

@Component({
    selector: 'sales-cmp',
    templateUrl: 'sales.component.html',
    styleUrls: ['sales.component.scss']
})

export class SalesComponent implements OnInit, OnDestroy {

    public sales: Sale[] = [];

    constructor(private translate: TranslateService, private _store: Store<appState.AppState>) { }

    ngOnInit() {
        this._store.select(appState.getSales).pipe(untilDestroyed(this)).subscribe(sales => {
            this.sales = sales;
        });
    }

    public getSaleItems(sale: Sale): string {
        return sale.items.map(item => this.translate.instant(item.name)).join(', ');
    }

    public getDiscounts(sale: Sale): string {
        return sale.combos.map(combo => `${this.translate.instant(sale.items.find(item => item.id === combo.itemToBeDiscounted).name)} ${combo.percentageDiscount}%`).join(', ')
    }

    ngOnDestroy() { }
}

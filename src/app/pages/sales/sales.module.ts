import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SalesComponent } from './sales.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { SalesRoutingModule } from './sales.module.routing';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        TranslateModule,
        SalesRoutingModule
    ],
    declarations: [
        SalesComponent
    ]
})

export class SalesModule { }

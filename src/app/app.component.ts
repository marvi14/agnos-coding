import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  constructor(private translate: TranslateService) { }

  ngOnInit(): void {
    //We're hardcoing ES to the language, but we can perfectly take the browser default or whatever logic we want
    this.translate.setDefaultLang('es');
    this.translate.use('es');
  }
}

import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { ROUTES } from '../constants/routes.const';
import { Item } from '../interfaces/item.interface';
import { Store } from '@ngrx/store';
import * as appState from '../../store/reducers.index';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { cloneDeep } from 'lodash';
import { AddOneToItemInCart, Checkout, RemoveOneToItemInCart } from 'app/store/shop/shop.actions';
import { Combo } from 'app/store/shop/shop.reducer';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  moduleId: module.id,
  selector: 'navbar-cmp',
  templateUrl: 'navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit, OnDestroy {
  private listTitles: any[];
  location: Location;
  private nativeElement: Node;
  private toggleButton;
  private sidebarVisible: boolean;
  public cartBadge = 0;
  public cart: Item[] = [];
  public combos: Combo[] = [];

  public isCollapsed = true;
  @ViewChild("navbar-cmp", { static: false }) button;

  constructor(location: Location, private spinner: NgxSpinnerService, private toastr: ToastrService, public translate: TranslateService, private element: ElementRef, private router: Router, private _store: Store<appState.AppState>) {
    this.location = location;
    this.nativeElement = element.nativeElement;
    this.sidebarVisible = false;

    this._store.select(appState.getCart).pipe(untilDestroyed(this)).subscribe(cart => {
      this.cart = cloneDeep(cart);
      this.cartBadge = this.cart.reduce((total, { selectedAmount }) => total + selectedAmount, 0);
    });

    this._store.select(appState.getPotentialCombos).pipe(untilDestroyed(this)).subscribe(combosData => {
      this.combos = combosData.combos;
    });
  }

  ngOnInit() {
    this.listTitles = ROUTES;
    var navbar: HTMLElement = this.element.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
    this.router.events.subscribe((event) => {
      this.sidebarClose();
    });
  }

  getTitle() {
    const title = `/app/${this.location.prepareExternalUrl(this.location.path()).split('/')[2]}`;
    for (var item = 0; item < this.listTitles.length; item++) {
      if (this.listTitles[item].path === title) {
        return this.translate.instant(this.listTitles[item].title);
      }
    }
  }

  public addItem(item: Item) {
    this._store.dispatch(new AddOneToItemInCart(item.id));
  }

  public removeItem(item: Item) {
    this._store.dispatch(new RemoveOneToItemInCart(item.id));
  }

  public calculateCartTotalAmount(): number {
    return this.cart.reduce((acc, item) => {
      return acc += (item.price * item.selectedAmount);
    }, 0);
  }

  public calculateCartTaxAmount(): number {
    return this.cart.reduce((acc, item) => {
      //Tax is applied on the product
      const unitTaxToBeCollected = (item.taxPercentage * item.price) / 100;
      return acc += (unitTaxToBeCollected * item.selectedAmount);
    }, 0);
  }

  public getComboName(combo: Combo): string {
    const discountedProductName = this.translate.instant(this.cart.find(item => item.id === combo.itemToBeDiscounted).name);
    return this.translate.instant('CART.COMBO_NAME', { product: discountedProductName, discount: combo.percentageDiscount });
  }

  public getComboDiscount(combo: Combo): number {
    //Discount on the actual item price
    const cartItem = this.cart.find(item => item.id === combo.itemToBeDiscounted);
    return (cartItem.price * (combo.percentageDiscount / 100)) * cartItem.selectedAmount;
  }

  public calculateTotalDiscounts(): number {
    return this.combos.reduce((acc, combo) => {
      return acc += this.getComboDiscount(combo);
    }, 0);
  }

  public checkout() {
    if (this.cart.length > 0) {
      //Simulating API call to perform checkout
      this.spinner.show();
      setTimeout(() => {
        this._store.dispatch(new Checkout(this.checkoutTotal));
        this.spinner.hide();
        this.toastr.success(this.translate.instant('CART.CHECKOUT_OK'));
      }, 1000);
    }
  }

  public get checkoutTotal(): number {
    return (this.calculateCartTotalAmount() + this.calculateCartTaxAmount() - this.calculateTotalDiscounts())
  }

  sidebarToggle() {
    if (this.sidebarVisible === false) {
      this.sidebarOpen();
    } else {
      this.sidebarClose();
    }
  }

  sidebarOpen() {
    const toggleButton = this.toggleButton;
    const html = document.getElementsByTagName('html')[0];
    const mainPanel = <HTMLElement>document.getElementsByClassName('main-panel')[0];
    setTimeout(function () {
      toggleButton.classList.add('toggled');
    }, 500);

    html.classList.add('nav-open');
    if (window.innerWidth < 991) {
      mainPanel.style.position = 'fixed';
    }
    this.sidebarVisible = true;
  };

  sidebarClose() {
    const html = document.getElementsByTagName('html')[0];
    const mainPanel = <HTMLElement>document.getElementsByClassName('main-panel')[0];
    if (window.innerWidth < 991) {
      setTimeout(function () {
        mainPanel.style.position = '';
      }, 500);
    }
    this.toggleButton.classList.remove('toggled');
    this.sidebarVisible = false;
    html.classList.remove('nav-open');
  };

  collapse() {
    this.isCollapsed = !this.isCollapsed;
    const navbar = document.getElementsByTagName('nav')[0];
    console.log(navbar);
    if (!this.isCollapsed) {
      navbar.classList.remove('navbar-transparent');
      navbar.classList.add('bg-white');
    } else {
      navbar.classList.add('navbar-transparent');
      navbar.classList.remove('bg-white');
    }
  }

  ngOnDestroy() { }

}

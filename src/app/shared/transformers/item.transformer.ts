import { Item, ItemApiResponse } from "../interfaces/item.interface";

//Applying the Adapter Design Pattern for handling API responses (https://www.dofactory.com/javascript/design-patterns/adapter)
export function transformItemsFromApi(apiResponse: ItemApiResponse[]): Item[] {
    const items: Item[] = [];
    apiResponse.forEach(apiItem => {
        items.push({
            amount: apiItem.amount,
            description: apiItem.description,
            id: apiItem.id,
            name: apiItem.name,
            price: apiItem.item_price,
            taxPercentage: apiItem.tax_percentage,
            selectedAmount: 1
        });
    });
    return items;
}
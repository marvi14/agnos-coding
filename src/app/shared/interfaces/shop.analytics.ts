export interface ShopAnalytics {
    stock: number;
    revenue: number;
    sales: number;
}
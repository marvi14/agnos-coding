export interface Item {
    id: ITEM_ID;
    name: string;
    description: string;
    taxPercentage: number;
    price: number;
    amount: number;
    selectedAmount: number;
}

export interface ItemApiResponse {
    id: number;
    name: string;
    description: string;
    tax_percentage: number;
    item_price: number;
    amount: number;
}

export enum ITEM_ID {
    BREAD = 0,
    JUICE = 1,
    MILK = 2,
    COFEE = 3
}
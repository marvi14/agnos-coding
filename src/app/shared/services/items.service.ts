import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Item, ItemApiResponse } from "../interfaces/item.interface";
import { map } from 'rxjs/operators';
import { transformItemsFromApi } from "../transformers/item.transformer";

@Injectable()
export class ItemsService {

    //This API URL can come from a Config File
    private itemsUrl: string = './assets/data/items.json';

    constructor(private http: HttpClient) { }

    public getItems(): Observable<Item[]> {
        return this.http.get(this.itemsUrl).pipe(map((response: ItemApiResponse[]) => {
            //We transform the API response to our own model
            return transformItemsFromApi(response);
        }));
    }

}
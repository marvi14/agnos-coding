import { RouteInfo } from "../interfaces/route-info.interface";

export const ROUTES: RouteInfo[] = [
    { path: '/app/dashboard', title: 'Dashboard', icon: 'nc-bank', class: '' },
    { path: '/app/sales', title: 'Sales', icon: 'nc-money-coins', class: '' }
];
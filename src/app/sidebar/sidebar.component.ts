import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ROUTES } from '../shared/constants/routes.const';
import { RouteInfo } from '../shared/interfaces/route-info.interface';

@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {

    public menuItems: RouteInfo[];

    constructor() { }

    ngOnInit() {
        this.menuItems = ROUTES;
    }
}
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToastrModule } from "ngx-toastr";
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule } from './shared/navbar/navbar.module';
import { AppComponent } from './app.component';
import { AppRoutes, AppRoutingModule } from './app.routing';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { AdminLayoutModule } from "./layouts/admin-layout/admin-layout.module";
import { ItemsService } from "./shared/services/items.service";
import { NgxSpinnerModule } from "ngx-spinner";
import { storeFreeze } from 'ngrx-store-freeze';
import { MetaReducer, StoreModule } from "@ngrx/store";
import { environment } from "../environments/environment";
import { reducers } from './store/reducers.index';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}

export const metaReducers: MetaReducer<{}>[] = !environment.production ? [storeFreeze] : [];

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent
  ],
  imports: [
    BrowserAnimationsModule,
    SidebarModule,
    NavbarModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true
    }),
    FooterModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    AppRoutingModule,
    AdminLayoutModule,
    NgxSpinnerModule,
    StoreModule.forRoot({}, { metaReducers }),
    StoreModule.forFeature('app', reducers),
  ],
  providers: [ItemsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
